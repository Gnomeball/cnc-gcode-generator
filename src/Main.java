import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import static java.lang.Math.*;

class Main {

    static int printSpeed = 0;
    static int travelSpeed = 0;
    static int laserPower = 0;

    private static int linesPerMil = 0;

    private static double totalPrintDistance;
    private static double totalTravelDistance;
    private static double totalTime;
    private static double efficiency;

    public static void main(String[] args) {

        Scanner in;
        String path = null;
        File outputFile;
        PrintWriter out = null;
        String outputFileName;
        BufferedImage inputImage = null;

        try {
            in = new Scanner(System.in);

            System.out.print("File Path : ");
            path = in.nextLine();
            if (path.equals("")) {
                System.out.println("Path incorrect.");
                System.exit(-1);
            }

            System.out.print("Output Name : ");
            outputFileName = in.nextLine();
            if (outputFileName.equals("")) {
                System.out.println("Output File incorrect.");
                System.exit(-1);
            }

            System.out.print("Print Speed : ");
            printSpeed = Integer.parseInt(in.nextLine());
            if (printSpeed == 0) {
                System.out.println("Print Speed incorrect.");
                System.exit(-1);
            }

            System.out.print("Travel Speed : ");
            travelSpeed = Integer.parseInt(in.nextLine());
            if (travelSpeed == 0) {
                System.out.println("Travel Speed incorrect.");
                System.exit(-1);
            }

            System.out.print("Laser Intensity : ");
            laserPower = Integer.parseInt(in.nextLine());
            if (laserPower == 0) {
                System.out.println("Laser Power incorrect.");
                System.exit(-1);
            }

            System.out.print("Lines per Millimetre : ");
            linesPerMil = Integer.parseInt(in.nextLine());
            if (linesPerMil == 0) {
                System.out.println("Lines per Millimetre incorrect.");
                System.exit(-1);
            }

            in.close();

            inputImage = ImageIO.read(new File(path));
            outputFile = new File(outputFileName + ".gcode");

            out = new PrintWriter(outputFile);
            out.flush(); // Empty the file

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        BufferedImage cleanedImage = cleanImage(inputImage);

        ArrayList<Line> lines = buildLines(cleanedImage);
        ArrayList<Line> outLines = orderLines(lines);

        writeFile(path, out, outLines);

    }

    private static BufferedImage cleanImage(BufferedImage image) {

        for (int y = 1; y < image.getHeight()-1; y++) {
            for (int x = 1; x < image.getWidth()-1; x++) {

                if (image.getRGB(x, y) != 0) {
                    image.setRGB(x, y, -16777216);
                }

            }
        }

        return image;

    }

    private static ArrayList<Line> buildLines(BufferedImage image) {

        ArrayList<Line> lines = new ArrayList<>();

        Line tempLine;

        Point tempStart = new Point();
        Point tempEnd;

        int previous_pixel = 0;
        int density = 10 / linesPerMil;

        int[][] imageArray = new int[image.getHeight()][image.getWidth()];

        for (int y = 0 ; y < image.getHeight() ; y += density) {
            for (int x = 0 ; x < image.getWidth() ; x++) {

                //int pixel = image.getRGB(x, y);

                int pixel = image.getRGB(x, y);

                imageArray[y][x] = pixel == 0 ? 0 : 1;

                if (previous_pixel != pixel) {

                    if (pixel == -16777216) {  // BLACK - start line
                        tempStart = new Point(x, image.getHeight() - (y - 1));
                    } else if (pixel == 0) {  // NO COLOUR - end line
                        tempEnd = new Point(x, image.getHeight() - (y - 1));

                        tempLine = new Line(tempStart, tempEnd);
                        lines.add(tempLine);
                    }

                    previous_pixel = pixel;
                }

            }
        }

        log(imageArray);

        return lines;
    }

    private static void log(int[][] image) {

        PrintWriter pw;

        try {
            pw = new PrintWriter(new File("log.txt"));
            pw.flush();

            for (int i = 0 ; i < image.length ; i += 2) {
                int[] row = image[i];
                for (int pixel : row) {
                    pw.print(pixel);
                }
                pw.println();
            }

            pw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static ArrayList<Line> cleanLines(ArrayList<Line> lines) {

        ArrayList<Line> temp = new ArrayList<>();

        for (Line l : lines) {
            if (l.getLength() > 1) {
                temp.add(l);
            }
        }

        return temp;

    }

    private static ArrayList<Line> orderLines(ArrayList<Line> lines) {

        ArrayList<Line> outLines = new ArrayList<>();
        Line previous;
        double start;
        double end;
        double distanceFromPrevious;
        double minimumTravelDistance = 1000;
        Line closest = null;

        lines = cleanLines(lines);

        outLines.add(new Line(new Point(0, 0), new Point(0, 0)));
        Collections.reverse(lines);

        while (lines.size() > 0) {

            for (Line line : lines) {
                previous = outLines.get(outLines.size() - 1);

                start = sqrt(
                    pow(abs((line.getStart().getX() - previous.getEnd().getX())), 2) +
                    pow(abs((line.getStart().getY() - previous.getEnd().getY())), 2));

                end = sqrt(
                    pow(abs((line.getEnd().getX() - previous.getEnd().getX())), 2) +
                    pow(abs((line.getEnd().getY() - previous.getEnd().getY())), 2));

                if (end < start) {
                    line.swapPoints();
                }

                distanceFromPrevious = min(start, end);

                if (distanceFromPrevious < minimumTravelDistance) {
                    closest = line;
                    minimumTravelDistance = distanceFromPrevious;
                }
            }

            // Add travel distance and time
            totalTravelDistance += minimumTravelDistance;
            totalTime += minimumTravelDistance / travelSpeed;

            // Add line distance and time
            assert closest != null;
            double length = closest.getLength();
            totalPrintDistance += length;
            totalTime += length / printSpeed;

            // Calculate efficiency
            efficiency = totalPrintDistance / (totalPrintDistance + totalTravelDistance) * 100;

            // Transfer line to output
            outLines.add(lines.get(lines.indexOf(closest)));
            lines.remove(closest);

            // Reset max distance
            minimumTravelDistance = 1000;

        }

        outLines.remove(0);

        return outLines;
    }

    private static void writeFile(String path, PrintWriter out, ArrayList<Line> lines) {

        out.println("; Super Terrible GCode Generator Mk5");
        out.println();
        out.println("; File : " + path.split("/")[path.split("/").length - 1]);
        out.println();
        out.println("; Print Speed\t: " + printSpeed);
        out.println("; Travel Speed\t: " + travelSpeed);
        out.println("; Laser Power\t: " + laserPower);
        out.println("; Lines per mm\t: " + linesPerMil);
        out.println();
        out.println("; Total Print Distance\t: " + round(totalPrintDistance) / 10 + " mm");
        out.println("; Total Travel Distance\t: " + round(totalTravelDistance) / 10 + " mm");
        out.println();
        out.println("; Estimated Total Time\t: " + round(totalTime) / 10 + " minutes");
        out.println();
        out.println("; Print Efficiency\t: " + round(efficiency) + " %");
        out.println();
        out.println("; ABSOLUTE POSITIONING");
        out.println("G90");
        out.println();
        out.println("; START IMAGE");
        out.println();

        for (Line l : lines) {
            out.print(l.printLine());
        }

        out.println();
        out.println("; END IMAGE");
        out.println();
        out.println("G28");

        out.close();

    }
}
