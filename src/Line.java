import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

class Line {

    private Point start;
    private Point end;

    Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    Point getStart() {
        return start;
    }

    Point getEnd() {
        return end;
    }

    double getLength() {
        return sqrt(
            pow(abs(this.end.getX() - this.start.getX()), 2) +
            pow(abs(this.end.getY() - this.start.getY()), 2));
    }

    void swapPoints() {
        Point temp = this.start;
        this.start = this.end;
        this.end = temp;
    }

    String printLine() {
        return
            "G90 G1 X" + start.getX() / 10 + " Y" + start.getY() / 10 + " F" + Main.travelSpeed + "\n" +
            "M03 S" + Main.laserPower + "\n" +
            "G90 G1 X" + end.getX() / 10 + " Y" + end.getY() / 10 + " F" + Main.printSpeed + "\n" +
            "M05" + "\n";
    }

}
