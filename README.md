# SuperTerribleGCodeGenerator

This project is now depreceated.

New version can be found at [CNC-Laser-GCode-Generator](https://gitlab.com/Gnomeball/cnc-laser-gcode-generator)

I needed a thing to turn images into useful GCode for my home made CNC Laser Engraver so I wrote this, it's extremely basic and quite honestly needs some updating.

It does work however.

![Example Output](Example.png)
